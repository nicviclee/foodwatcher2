package foodwatcher.spr.com.foodwatcher2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jwang on 15-03-14.
 */
public class Herbs extends FoodType {
    private static Herbs instance;

    public Herbs() {
    }

    public static Herbs getInstance() {
        if (instance != null)
            return instance;
        else {
            instance = new Herbs();

            instance.setName("Herbs");
            FoodCategory herbs = new FoodCategory("Herbs");
            FoodCategory spices = new FoodCategory("Spices");

            List<FoodCategory> items = new ArrayList<FoodCategory>();
            items.add(herbs);
            items.add(spices);
            instance.setCategories(items);

            return instance;
        }
    }
}
