package foodwatcher.spr.com.foodwatcher2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class Home extends Activity {

    Button buyGroceriesButton = null;
    Button transitionToInventoryActivityButton = null;
    Button transitionToAddFoodActivityButton = null;
    Button transitionToUseRecipeActivityButton = null;
    Button transitionToGroceryListButton = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        buyGroceriesButton = (Button)findViewById(R.id.addItemsToInventoryButton);
        buyGroceriesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Home.this, AddFoodItemActivity.class);
                startActivity(i);
            }
        });

        transitionToInventoryActivityButton = (Button)findViewById(R.id.inventoryButton);
        transitionToInventoryActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Home.this, InventoryActivity.class);
                startActivity(i);
            }
        });

        transitionToAddFoodActivityButton = (Button)findViewById(R.id.add_food_type_button);
        transitionToAddFoodActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Home.this, AddFoodTypeActivity.class);
                startActivity(i);
            }
        });

       transitionToGroceryListButton = (Button) findViewById(R.id.groceryListButton);
        transitionToGroceryListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Home.this, PredictGroceryListActivity.class);
                startActivity(i);
            }
        });

        transitionToUseRecipeActivityButton = (Button)findViewById(R.id.recipeButton);
        transitionToUseRecipeActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Intent i = new Intent(Home.this, UseRecipeActivity.class);
                 startActivity(i);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}