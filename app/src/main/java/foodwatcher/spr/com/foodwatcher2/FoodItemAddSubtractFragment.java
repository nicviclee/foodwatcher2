package foodwatcher.spr.com.foodwatcher2;

import android.content.AbstractThreadedSyncAdapter;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class FoodItemAddSubtractFragment extends Fragment {
    private String foodCategory;

    public FoodItemAddSubtractFragment() {
        // Required empty public constructor
    }

    public static Fragment createFoodItemAddSubtractFragment(String category) {
        FoodItemAddSubtractFragment newFragment = new FoodItemAddSubtractFragment();
        newFragment.setFoodCategory(category);

        return newFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_food_item_add_subtract, container, false);

        // fake data
        DatabaseHelper db = DatabaseHelper.getInstance(this.getActivity());
        List<AbstractFood> inputData = db.getAbstractFoodByCategory(foodCategory);

        FoodItemAddSubtractAdapter adapter = new FoodItemAddSubtractAdapter(getActivity(), inputData);
        // attach to listview
        ListView listView = (ListView) rootView.findViewById(R.id.listview_food_item_add_subtract);
        listView.setAdapter(adapter);
        return rootView;
    }

    // getters and setters
    public String getFoodType() {
        return this.foodCategory;
    }

    public void setFoodCategory(String newCategory) {
        this.foodCategory = newCategory;
    }

}
