package foodwatcher.spr.com.foodwatcher2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by jwang on 15-03-14.
 */

public class AddItemTabsAdapter extends FragmentPagerAdapter {

    private final int FOOD_TYPE_TABS = 5;

    public AddItemTabsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return AddFoodItemFragment.createAddFoodItemFragment(Fruits.getInstance());
            case 1:
                return AddFoodItemFragment.createAddFoodItemFragment(Grains.getInstance());
            case 2:
                return AddFoodItemFragment.createAddFoodItemFragment(MeatAndAlt.getInstance());
            case 3:
                return AddFoodItemFragment.createAddFoodItemFragment(Vegetables.getInstance());
            case 4:
                return AddFoodItemFragment.createAddFoodItemFragment(Herbs.getInstance());
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return FOOD_TYPE_TABS;
    }
}
