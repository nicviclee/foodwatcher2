package foodwatcher.spr.com.foodwatcher2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jwang on 15-03-14.
 */

public class MeatAndAlt extends FoodType{
    private static MeatAndAlt instance;

    public MeatAndAlt() {
    }

    public static MeatAndAlt getInstance() {
        if (instance != null)
            return instance;
        else {
            instance = new MeatAndAlt();

            instance.setName("Meat or Dairy");
            FoodCategory red = new FoodCategory("Red Meat");
            FoodCategory poultry = new FoodCategory("Poultry");
            FoodCategory nuts = new FoodCategory("Nuts");
            FoodCategory legumes = new FoodCategory("Legumes");
            FoodCategory dairy = new FoodCategory("Dairy");

            List<FoodCategory> items = new ArrayList<FoodCategory>();
            items.add(red);
            items.add(poultry);
            items.add(nuts);
            items.add(legumes);
            items.add(dairy);
            instance.setCategories(items);

            return instance;
        }
    }
}
