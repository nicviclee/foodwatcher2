package foodwatcher.spr.com.foodwatcher2;

/**
 * Created by jwang on 15-03-14.
 */
public class AbstractFood {
    private float quantityInStock;
    private String name;
    private FoodCategory category;
    private FoodType foodType;
    private String units;
    private FoodType type;

    static final String[] TYPES = {"Fruits","Grains","Herbs","MeatAndAlt","Vegetables"};
    static final String[] CATEGORIES = {"Fruit: Citrus","Fruit: Berry","Grain: Raw","Grain: Prepared",
            "Meat or Dairy: Seafood","Meat or Dairy: Red Meat", "other"};
    static final String[] UNITS = {"items","mL","L","mg","g","kg","oz","lb"};



    public FoodType getType() {
        return type;
    }

    public void setType(FoodType type) {
        this.type = type;
    }

    public AbstractFood(String foodName, FoodCategory parent, FoodType type, String units) {
        name = foodName;
        category = parent;
        this.type = type;
        this.units = units;
        quantityInStock = 0;
    }

    public AbstractFood(String foodName, FoodCategory parent, FoodType type,
                        String units, float quantityInStock) {
        name = foodName;
        category = parent;
        this.type = type;
        this.units = units;
        this.quantityInStock = quantityInStock;

    }

    // getters and setters

    public float getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(float quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public String getName() {
        return name;
    }

    public FoodCategory getCategory() {
        return category;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public void setCategory(FoodCategory newCategory) {
        this.category = newCategory;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public FoodType getFoodType () {return this.foodType;}

    public void setFoodType(FoodType foodType) {this.foodType = foodType;}
}
