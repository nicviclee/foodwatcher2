package foodwatcher.spr.com.foodwatcher2;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.List;

import foodwatcher.spr.com.foodwatcher2.Constants.CategoryNames;
import foodwatcher.spr.com.foodwatcher2.Constants.TypeNames;

/**
 * Created by sikander on 2015-03-14.
 */
public class InventoryActivity  extends Activity {

    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory);

//        AbstractFood abstractFood1 = new AbstractFood("Cheese", new FoodCategory("Dairy"),
//                AbstractFood.UNITS[0], new FoodType("Meat and Stuff"));
//        AbstractFood abstractFood2 = new AbstractFood("Spinach", new FoodCategory("Leafy"),
//                AbstractFood.UNITS[4], new FoodType("Vegetables"));
//        AbstractFood abstractFood3 = new AbstractFood("Kidney Beans", new FoodCategory("Beans"),
//                AbstractFood.UNITS[7], new FoodType("Meat and Stuff"));
//        AbstractFood abstractFood4 = new AbstractFood("Squash", new FoodCategory("Leafy"),
//                AbstractFood.UNITS[0], new FoodType("Vegetable"));
//
//        Food food1 = new Food(new Float(213), abstractFood1);
//        Food food2 = new Food(new Float(33), abstractFood2);
//        Food food3 = new Food(new Float(1123), abstractFood3);
//        Food food4 = new Food(new Float(99), abstractFood4);
//
//        Food inventory[] = {food1, food2, food3, food4};
//        ArrayList<Food> inventoryList = new ArrayList<Food>(Arrays.asList(inventory));



        db = DatabaseHelper.getInstance(this);




        // TODO: Add in the database of actual food things


        List<Food> inventoryList = db.getAllFoodInstances();

        FoodItemAdapter foodItemAdapterAdapter = new FoodItemAdapter(this, inventoryList);
        ListView inventoryDisplay = (ListView) findViewById(R.id.listview_inventory);
        inventoryDisplay.setAdapter(foodItemAdapterAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
