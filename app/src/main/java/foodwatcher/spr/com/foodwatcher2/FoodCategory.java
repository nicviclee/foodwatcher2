package foodwatcher.spr.com.foodwatcher2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jwang on 15-03-14.
 */
public class FoodCategory {
    private String name;
    private List<AbstractFood> foodList;

    public FoodCategory(String categoryName) {
        name = categoryName;
        foodList = new ArrayList<AbstractFood>();
    }

    // getters and setters
    public String getName() {
        return this.name;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public List<AbstractFood> getFoodList() {
        return this.foodList;
    }

}
