package foodwatcher.spr.com.foodwatcher2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jwang on 15-03-14.
 */
public class Grains extends FoodType {
    private static Grains instance;
    private static final String name = "Grains";
    public Grains() {
    }

    public static Grains getInstance() {
        if (instance != null)
            return instance;
        else {
            instance = new Grains();

            instance.setName("Grains");
            FoodCategory raw = new FoodCategory("Raw");
            FoodCategory prepared = new FoodCategory("Prepared");

            List<FoodCategory> items = new ArrayList<FoodCategory>();
            items.add(raw);
            items.add(prepared);
            instance.setCategories(items);

            return instance;
        }
    }
}

