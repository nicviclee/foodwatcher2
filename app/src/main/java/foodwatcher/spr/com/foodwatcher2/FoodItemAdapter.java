package foodwatcher.spr.com.foodwatcher2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

/**
 * Created by sikander on 2015-03-14.
 */
public class FoodItemAdapter extends ArrayAdapter<Food> {


    public FoodItemAdapter(Context context, List<Food> foodItems) {
        super(context, 0, foodItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final Food foodItem = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.inventory_table_row, parent, false);
        }

        Button consumeButton = (Button) convertView.findViewById(R.id.inventoryConsumeFood);
        final TextView inventoryRowName = (TextView) convertView.findViewById(R.id.inventoryRowName);
        final EditText inventoryRowQuantity = (EditText) convertView.findViewById(R.id.inventoryConsumeFoodQuantity);
        TextView inventoryRowAge = (TextView) convertView.findViewById(R.id.inventoryRowAge);
        TextView inventoryRowUnits = (TextView) convertView.findViewById(R.id.inventoryRowUnits);

        String date = foodItem.getDate();
        final String name = foodItem.getAbstractFood().getName();
        final Float quantityRemaining = foodItem.getQuantity();
        final String unit = foodItem.getAbstractFood().getUnits();

        inventoryRowName.setText(name + " (" + quantityRemaining + " " + unit + ")");
        inventoryRowUnits.setText(unit);
        inventoryRowAge.setText("Added on: " + date);

        final DatabaseHelper db = DatabaseHelper.getInstance(this.getContext());

        consumeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!inventoryRowQuantity.getText().toString().equals("")) {
                    Float consumedAmount = Float.parseFloat(inventoryRowQuantity.getText().toString());
                    Float amountRemaining = quantityRemaining - consumedAmount;
                    if (amountRemaining >= 0) {
                        db.consumeFood(name, consumedAmount);
                        foodItem.setQuantity(amountRemaining);
                        inventoryRowName.setText(name + " (" + amountRemaining + " " + unit + ")");
                        inventoryRowQuantity.setText("");
                        inventoryRowQuantity.clearFocus();

                    } }
            }
        });

        return convertView;
    }
}