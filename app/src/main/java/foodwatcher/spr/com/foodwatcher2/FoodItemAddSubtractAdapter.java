package foodwatcher.spr.com.foodwatcher2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;
/**
 * Created by jwang on 15-03-14.
 */
public class FoodItemAddSubtractAdapter extends ArrayAdapter<AbstractFood> {
    EditText change = null;
    TextView quantity = null;
    TextView units = null;
    public FoodItemAddSubtractAdapter(Context context, List<AbstractFood> foods) {
        super(context, 0, foods);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final AbstractFood food = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.food_item_add_subtract_list_element, parent, false);
        }

        final TextView foodName = (TextView) convertView.findViewById(R.id.food_item_add_subtract_name);
        foodName.setText(food.getName());

        units = (TextView) convertView.findViewById(R.id.food_item_add_subtract_quantity_units);
        units.setText(food.getUnits());

        quantity = (TextView) convertView.findViewById(R.id.food_item_add_subtract_quantity_display);
        quantity.setText(Float.toString(food.getQuantityInStock()));

        change = (EditText) convertView.findViewById(R.id.food_item_add_subtract_quantity_change);
        change.setText("0");

        Button adder = (Button) convertView.findViewById(R.id.food_item_add_subtract_add_button);
        adder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



             Float currentChange = Float.parseFloat(change.getText().toString());
             CharSequence newText = Float.toString(currentChange += 1.0f);
             change.setText(newText);
            }
        });

        Button submitter = (Button) convertView.findViewById(R.id.food_item_add_subtract_submit_button);
        submitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper db = DatabaseHelper.getInstance(getContext());
                Float newQuantity = Float.parseFloat(change.getText().toString());
                newQuantity += Float.parseFloat(quantity.getText().toString());
                Food inputFood = new Food(newQuantity, food);
                quantity.setText(Float.toString(newQuantity));
                change.setText(0.0 + "");
                db.updateRow(inputFood, newQuantity);
            }
        });
        // Return the completed view to render on screen
        return convertView;
    }
}
