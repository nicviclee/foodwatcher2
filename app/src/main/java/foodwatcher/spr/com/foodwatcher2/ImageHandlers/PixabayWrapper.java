package foodwatcher.spr.com.foodwatcher2.ImageHandlers;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Mike Fink on 2015-03-14.
 * Wrapper class to talk to Pixabay API.
 */
public class PixabayWrapper {

    private static final String USERNAME = "mikegfink0";
    private static final String APIKEY = "0bc02d262971df263a26";

    public PixabayWrapper() {}

    public static String getURL(String search) {

        String query = buildQuery(search);
        try {
            JSONObject jsonObject = JSONGetter.readJsonFromUrl(query);
            String imageURL = processJSON(jsonObject);
            return imageURL;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Error getting URL: ", e.getMessage());
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("Error getting JSON: ", e.getMessage());
            return null;
        }
    }

    private static String buildQuery(String search) {
        try {
            String encodedSearch = URLEncoder.encode(search, "UTF-8");
            return "http://pixabay.com/api/?username=" + USERNAME + "&key=" + APIKEY +
                    "&q=" + encodedSearch + "&image_type=photo&orientation=horizontal";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Log.e("Should never reach. ", e.getMessage());
            return "Not a URL";
        }
    }

    private static String processJSON(JSONObject jsonObject) throws JSONException {
        JSONObject firstHit = jsonObject.getJSONArray("hits").getJSONObject(0);
        String imageURL;
        if (firstHit.has("thumbURL")) {
            imageURL = firstHit.getString("thumbURL");
        } else if (firstHit.has("previewURL")) {
            imageURL = firstHit.getString("previewURL");
        } else if (firstHit.has("imageURL")) {
            imageURL = firstHit.getString("imageURL");
        } else {
            throw new JSONException("Cannot find URL");
        }
        return imageURL;
    }
}
