package foodwatcher.spr.com.foodwatcher2;

import android.graphics.Bitmap;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteConstraintException;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import foodwatcher.spr.com.foodwatcher2.ImageHandlers.ImageDownloader;
import foodwatcher.spr.com.foodwatcher2.ImageHandlers.ImageManager;
import foodwatcher.spr.com.foodwatcher2.ImageHandlers.PixabayWrapper;

// STUB CLASS: produces view for adding a food type to database

public class AddFoodTypeActivity extends ActionBarActivity {

    private DatabaseHelper db = null;

    private Button doneButton;
    private Button cancelButton;
    private static Bitmap bitmap;
    private ImageView imageView;
    private TextView percent;
    private static EditText name;
    private static EditText quant;

    Spinner typeSpinner = null;
    Spinner unitsSpinner = null;
    Spinner categorySpinner = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food_type);

        db = DatabaseHelper.getInstance(this);
        // Set up UI elements
        initializeButtons();
        // TODO: research and set up dynamic spinner for categories & units
        initializeFoodViews();
        initializeSpinners();
    }

    private void initializeFoodViews() {
        quant = (EditText) findViewById(R.id.quantEntered);
        name = (EditText) findViewById(R.id.inputName);
        name.clearFocus();
        imageView = (ImageView) findViewById(R.id.image);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        percent = (TextView) findViewById(R.id.tvPercent);
        percent.setVisibility(View.INVISIBLE);
        bitmap = null;
    }

    private void initializeSpinners() {

        // Grain: raw/prepared
        // Seafood nuts beans red meat white meat
        // Leafy, mushrooms, root, bulb, cabbage,

        typeSpinner = (Spinner) findViewById(R.id.typeSpinner);
        categorySpinner = (Spinner) findViewById(R.id.categorySpinner);
        unitsSpinner = (Spinner) findViewById(R.id.unitsSpinner);

        ArrayAdapter<String> typesAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, AbstractFood.TYPES);
        typeSpinner.setAdapter(typesAdapter);

        Spinner categoriesSpinner = (Spinner) findViewById(R.id.categorySpinner);
        ArrayAdapter<String> categoriesAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, AbstractFood.CATEGORIES);
        categorySpinner.setAdapter(categoriesAdapter);

        Spinner unitSpinner = (Spinner) findViewById(R.id.unitsSpinner);
        ArrayAdapter<String> unitsAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, AbstractFood.UNITS);
        unitSpinner.setAdapter(unitsAdapter);

        name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    ImageDownloader imgDownloader =
                            new ImageDownloader(AddFoodTypeActivity.name.getText().
                                    toString().trim(), doneButton, imageView, percent, bitmap,
                                    new ImageDownloader.ImageLoaderListener() {
                                        @Override
                                        public void onImageDownloaded(Bitmap bmp) {
                                            AddFoodTypeActivity.bitmap = bmp;
                                        }
                                    });
                    imgDownloader.execute();
                }
            }
        });
    }

    private void initializeButtons() {
        doneButton = (Button)findViewById(R.id.doneButton);
        cancelButton = (Button)findViewById(R.id.cancelButton);

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // editText

                if (bitmap != null) {
                    ImageManager.saveFoodImage(bitmap, name.getText().toString());
                }

                // Prevent entry without name
                if (name.getText().toString().equals("")) {
                    // make alert dialog informing user
                    showAlertMessage( "Please enter food name");
                    return;
                }

                if (quant.getText().toString().equals("")) {
                    showAlertMessage( "Please enter a quantity");
                    return;
                }

                addToDatabase();
                // PROBLEM IN CONTROL FLOW
                finish();

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void addToDatabase() {
        ContentValues cv = new ContentValues(4);


        Spinner type = (Spinner) findViewById(R.id.typeSpinner);
        Spinner category = (Spinner) findViewById(R.id.categorySpinner);
        Spinner units = (Spinner) findViewById(R.id.unitsSpinner);

        db.addAbstractFoodToDatabase(new AbstractFood(name.getText().toString(),
                new FoodCategory(category.getSelectedItem().toString()),
                FoodType.findFoodType(category.getSelectedItem().toString()),
                units.getSelectedItem().toString()));

    }

    private void showAlertMessage(String msg) {
        //offsets down far enough to be visible to user
        int yoffset = 200;
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_VERTICAL, 0, yoffset);
        toast.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_food_type, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}