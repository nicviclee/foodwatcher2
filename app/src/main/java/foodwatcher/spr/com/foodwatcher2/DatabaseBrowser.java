package foodwatcher.spr.com.foodwatcher2;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;


public class DatabaseBrowser extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.debug_database_layout);

        if (getFragmentManager().findFragmentById(android.R.id.content)==null) {
            getFragmentManager().beginTransaction()
                    .add(android.R.id.content,
                            new DatabaseBrowserFragment()).commit();
        }

    }

}
