package foodwatcher.spr.com.foodwatcher2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sikander on 2015-03-14.
 */
public class Food {


    private Float quantity;
    private AbstractFood abstractFood;
    private String date;

    public Food(Float quantity, AbstractFood abstractFood) {

        this.quantity = quantity;
        this.abstractFood = abstractFood;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        this.date = dateFormat.format(date);
    }


    public Float getQuantity(){
        return quantity;
    }

    public Food(float quantity, AbstractFood abstractFood, String date) {
        this.quantity = quantity;
        this.abstractFood = abstractFood;
        this.date = date;
    }

    public AbstractFood getAbstractFood() {
        return abstractFood;
    }

    public String getDate() {
        return date;
    }

    public void setAbstractFood(AbstractFood abstractFood) {
        this.abstractFood = abstractFood;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }
}
