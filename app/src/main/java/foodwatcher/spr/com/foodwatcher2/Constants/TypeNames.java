package foodwatcher.spr.com.foodwatcher2.Constants;

/**
 * Created by mikefink on 2015-03-15.
 */
public class TypeNames {

    public static final String MEAT = "Meat, Dairy and Alternatives";
    public static final String FRUIT = "Fruit";
    public static final String VEG = "Vegetables";
    public static final String GRAINS = "Grains";
    public static final String HS = "Herbs and Spices";

}
