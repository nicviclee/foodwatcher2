package foodwatcher.spr.com.foodwatcher2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jwang on 15-03-14.
 */
public class FoodType {
    private String name;
    private List<FoodCategory> categories;

    public FoodType() {
        categories = new ArrayList<FoodCategory>();
    }

    public static FoodType findFoodType(String typeName) {
        switch (typeName.toLowerCase().trim()) {
            case "fruits":
                return Fruits.getInstance();
            case "grain":
                return Grains.getInstance();
            case "meat or dairy":
                return MeatAndAlt.getInstance();
            case "vegetable":
                return Vegetables.getInstance();
            case "herbs":
                return Herbs.getInstance();
            default:
                return null;
        }
    }

    public List<FoodCategory> getCategories() {
        return this.categories;
    }

    public void setCategories(List<FoodCategory> categories) {
        this.categories = categories;
    }

    public String getName() {
        return name;
    }

    public void setName(String typeName) {
        name = typeName;
    }

}
