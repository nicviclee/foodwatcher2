package foodwatcher.spr.com.foodwatcher2.ImageHandlers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by mikefink on 2015-03-14.
 */

public class ImageDownloader extends AsyncTask<Void, Integer, Void> {

    private String search;
    private Button save;
    //private Context context;
    private int progress;
    private ImageView imageView;
    private Bitmap bitmap;
    private TextView percent;
    private ImageLoaderListener listener;

    public ImageDownloader(String search, Button save, ImageView img, TextView percent,
                           Bitmap bitmap, ImageLoaderListener listener) {
        this.search = search;
        this.save = save;
        //this.context = context;
        this.imageView = img;
        this.percent = percent;
        this.bitmap = bitmap;
        this.listener = listener;
    }

    /*--- we need this interface for keeping the reference to our Bitmap from the MainActivity.
     *  Otherwise, bitmap would be null in our MainActivity*/
    public interface ImageLoaderListener {

        void onImageDownloaded(Bitmap bmp);

    }

    @Override
    protected void onPreExecute() {

        progress = 0;
        percent.setVisibility(View.VISIBLE);
        //Toast.makeText(context, "Starting download", Toast.LENGTH_SHORT).show();
        save.setEnabled(false);
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... arg0) {
        String imageURL = PixabayWrapper.getURL(search);
        if (imageURL != null) {
            bitmap = getBitmapFromURL(imageURL);

            while (progress < 100) {

                progress += 1;

                publishProgress(progress);

                SystemClock.sleep(100);
            }
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        percent.setText(values[0] + "%");

        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Void result) {

        if (listener != null) {
            listener.onImageDownloaded(bitmap);
        }
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        }
        save.setEnabled(true);
        //Toast.makeText(context, "Download Complete", Toast.LENGTH_SHORT).show();

        super.onPostExecute(result);
    }

    public static Bitmap getBitmapFromURL(String link) {
        try {
            URL url = new URL(link);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);

            return myBitmap;

        } catch (IOException e) {
            e.printStackTrace();
            Log.e("getBmpFromUrl error: ", e.getMessage());
            return null;
        }
    }

}