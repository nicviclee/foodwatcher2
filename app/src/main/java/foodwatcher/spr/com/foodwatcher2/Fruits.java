package foodwatcher.spr.com.foodwatcher2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jwang on 15-03-14.
 */
public class Fruits extends FoodType {
    private static Fruits instance;
    public Fruits() {
    }

    public static Fruits getInstance() {
        if (instance != null)
            return instance;
        else {
            instance = new Fruits();

            instance.setName("Fruits");
            FoodCategory tropical = new FoodCategory("Tropical");
            FoodCategory citrus = new FoodCategory("Citrus");
            FoodCategory berries = new FoodCategory("Berries");
            FoodCategory melons = new FoodCategory("Melons");
            FoodCategory pomes = new FoodCategory("Pomes");

            List<FoodCategory> items = new ArrayList<FoodCategory>();
            items.add(tropical);
            items.add(citrus);
            items.add(berries);
            items.add(melons);
            items.add(pomes);
            instance.setCategories(items);

            return instance;
        }
    }
}
