package foodwatcher.spr.com.foodwatcher2;


import android.app.ListFragment;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class DatabaseBrowserFragment extends ListFragment {


    private DatabaseHelper db=null;
    private Cursor current=null;


    public DatabaseBrowserFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        setRetainInstance(true);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SimpleCursorAdapter adapter=
                new SimpleCursorAdapter(getActivity(), R.layout.row,
                        current, new String[] {
                        DatabaseHelper.NAME,
                        DatabaseHelper.UNITS },
                        new int[] { R.id.title, R.id.value },
                        0);

        setListAdapter(adapter);

        if (current==null) {
            db=DatabaseHelper.getInstance(getActivity());
            new LoadCursorTask().execute();
        }
    }

    abstract private class BaseTask<T> extends AsyncTask<T, Void, Cursor> {
        @Override
        public void onPostExecute(Cursor result) {
            ((CursorAdapter)getListAdapter()).changeCursor(result);
        }

        protected Cursor doQuery() {
            Cursor result=
                    db
                            .getReadableDatabase()
                            .query(DatabaseHelper.NAME,
                                    new String[] {"ROWID AS _id",
                                            DatabaseHelper.NAME,
                                            DatabaseHelper.UNITS},
                                    null, null, null, null, DatabaseHelper.ABSTRACT_TABLE);

            result.getCount();

            return(result);
        }
    }

    private class LoadCursorTask extends BaseTask<Void> {
        @Override
        protected Cursor doInBackground(Void... params) {
            return(doQuery());
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        ((CursorAdapter)getListAdapter()).getCursor().close();
        db.close();
    }


}
