package foodwatcher.spr.com.foodwatcher2;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by jwang on 15-03-14.
 */
public class FoodCategoryAdapter extends ArrayAdapter<FoodCategory> {

    private Context context;
    public FoodCategoryAdapter(Context context, List<FoodCategory> categories) {
        super(context, 0, categories);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final FoodCategory category = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.add_food_item_list_element, parent, false);
        }
        // Lookup view for data population
        TextView categoryName = (TextView) convertView.findViewById(R.id.add_food_item_listview_textview);
        // Populate the data into the template view using the data object
        categoryName.setText(category.getName());
        // Set the acivity that is launched when this textview is clicked
        categoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView view = (TextView) v;
                Intent addSubtractIntent = new Intent(context, FoodItemAddSubtractActivity.class).putExtra("category", ((TextView) v).getText());
                context.startActivity(addSubtractIntent);
            }
        });
        // Return the completed view to render on screen
        return convertView;
    }
}

