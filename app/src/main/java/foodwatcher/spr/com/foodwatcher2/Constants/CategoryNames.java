package foodwatcher.spr.com.foodwatcher2.Constants;

/**
 * Created by mikefink on 2015-03-15.
 */
public class CategoryNames {
    public static final String FRUIT_CORE = "Core";
    public static final String FRUIT_CITRUS = "Citrus";
    public static final String FRUIT_TROPICAL = "Tropical";
    public static final String FRUIT_BERRIES = "Berries";
    public static final String FRUIT_MELONS = "Melons";
    public static final String FRUIT_PITTED = "Pitted";
    public static final String VEG_ROOT = "Root";
    public static final String VEG_LEAFY = "Leafy";
    public static final String VEG_BULB = "Bulb";
    public static final String VEG_FRUITS = "Fruits";
    public static final String VEG_MUSHROOMS = "Mushrooms";
    public static final String VEG_CABBAGE = "Cabbage";
    public static final String MEAT_SEAFOOD = "Seafood";
    public static final String MEAT_WHITE = "White Meat";
    public static final String MEAT_RED = "Red Meat";
    public static final String MEAT_NUTS = "Nuts";
    public static final String MEAT_BEANS = "Beans";
    public static final String MEAT_DAIRY = "Dairy";
    public static final String GRAINS_RAW = "Raw";
    public static final String GRAINS_PREPARED = "Prepared";
    public static final String HS_SPICES = "Spices";
    public static final String HS_HERBS = "Herbs";

    public CategoryNames(){}
}
