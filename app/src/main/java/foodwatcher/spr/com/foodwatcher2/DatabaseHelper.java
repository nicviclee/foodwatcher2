package foodwatcher.spr.com.foodwatcher2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import foodwatcher.spr.com.foodwatcher2.Constants.CategoryNames;
import foodwatcher.spr.com.foodwatcher2.Constants.TypeNames;

public class DatabaseHelper extends SQLiteOpenHelper {
    // TODO!!! change the name of the main database to something correct
    private static final String DATABASE_NAME = "TEMPTE.db";
    private static final int SCHEMA_VERSION = 1;
    private static DatabaseHelper singleton = null;



    static final String NAME ="name";
    static final String CATEGORY="category";
    static final String ABSTRACT_TABLE="abstractFood";
    static final String INVENTORY_TABLE="foodItems";
    static final String TYPE="type";
    static final String DATE="date";
    static final String UNITS="units";
    static final String QUANTITY="quantity";


    public static DatabaseHelper getInstance(Context ctxt) {

        if (singleton == null) {
            singleton = new DatabaseHelper(ctxt);
        }
        return singleton;
    }

    // Create unique instance of databaseHelper
    // Let SQLiteOpenHelper take care of retrieving or creating database as necessary
    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, SCHEMA_VERSION);
    }

    // Creates two tables in database the first time the application is opened
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + ABSTRACT_TABLE + " (" + NAME + " TEXT PRIMARY KEY, "
                + TYPE + " TEXT, "
                + CATEGORY + " TEXT, "
                + UNITS + " TEXT);");

        // CREATE TABLE foodItems (name TEXT, date TEXT, quantity REAL, PRIMARY KEY(name, date), FOREIGN KEY(name) REFERENCES abstractFood(name));
        db.execSQL("CREATE TABLE " + INVENTORY_TABLE + " (" + NAME + " TEXT, "
                + DATE + " TEXT, "
                + QUANTITY + " REAL," +
                " PRIMARY KEY(" + NAME + ", " + DATE + ")," +
                " FOREIGN KEY(" + NAME + ") " +
                "REFERENCES " + ABSTRACT_TABLE + "(" + NAME + "));");

        /*AbstractFood abstractFood = new AbstractFood("Apple", new FoodCategory(CategoryNames.FRUIT_CORE),
                new FoodType(TypeNames.FRUIT), "items");
        addAbstractFoodToDatabase(abstractFood);
        abstractFood = new AbstractFood("Milk", new FoodCategory(CategoryNames.MEAT_DAIRY),
                new FoodType(TypeNames.MEAT), "mL");
        addAbstractFoodToDatabase(abstractFood);
        abstractFood = new AbstractFood("Carrot", new FoodCategory(CategoryNames.VEG_ROOT),
                new FoodType(TypeNames.VEG), "items");
        addAbstractFoodToDatabase(abstractFood);
        abstractFood = new AbstractFood("Rice", new FoodCategory(CategoryNames.GRAINS_RAW),
                new FoodType(TypeNames.GRAINS), "kg");
        addAbstractFoodToDatabase(abstractFood);*/



        // TODO: Add in the database of actual food things


    }

    public List<Food> getFoodInstance(String name) {
        String query = "SELECT * FROM " + ABSTRACT_TABLE +
                " NATURAL JOIN " + INVENTORY_TABLE + " where name=?";

        SQLiteDatabase dbs = getReadableDatabase();
        Cursor cursor = dbs.rawQuery(query, new String[]{name});


        List<Food> foodList = new ArrayList<>();

        while (cursor.moveToNext()) {
            Food food = makeFoodFromCursor(cursor);
            foodList.add(food);
        }
        // Don't close before this or crash
        dbs.close();
        return foodList;
    }

    private Food makeFoodFromCursor(Cursor cursor) {
        String foodName = cursor.getString(cursor.getColumnIndex(NAME));
        String foodDate = cursor.getString(cursor.getColumnIndex(DATE));
        FoodType foodType = FoodType.findFoodType(cursor.getString(cursor.getColumnIndex(TYPE)));
        FoodCategory foodCategory =
                new FoodCategory(cursor.getString(cursor.getColumnIndex(CATEGORY)));
        String foodUnits = cursor.getString(cursor.getColumnIndex(UNITS));
        float foodQuantity = cursor.getFloat(cursor.getColumnIndex(QUANTITY));

        AbstractFood abstractFood = new AbstractFood(foodName, foodCategory,
                foodType, foodUnits);

        return new Food(foodQuantity, abstractFood, foodDate);
    }

    public List<Food> getAllFoodInstances() {

        String query = "SELECT * FROM " + ABSTRACT_TABLE +
                " NATURAL JOIN " + INVENTORY_TABLE;

        String queryAll = "SELECT * FROM " + ABSTRACT_TABLE;

        AbstractFood abstractFood = new AbstractFood("Rice", new FoodCategory(CategoryNames.GRAINS_RAW),
                Grains.getInstance(), "kg");


        addAbstractFoodToDatabase(abstractFood);

        addFoodInstanceToDatabase(new Food(new Float(2), abstractFood));



        SQLiteDatabase dbs = getReadableDatabase();

        Cursor cursor = (Cursor)dbs.rawQuery(query, new String[]{});


        List<Food> foodList = new ArrayList<>();

        while ( cursor.moveToNext()) {
            Food food = makeFoodFromCursor(cursor);
            foodList.add(food);

        }
        dbs.close();
        return foodList;
    }

    public void addAbstractFoodToDatabase(AbstractFood abstractFood) {
        ContentValues values = new ContentValues(4);
        values.put(NAME, abstractFood.getName());
        values.put(TYPE, abstractFood.getName());
        values.put(CATEGORY, abstractFood.getCategory().getName());
        values.put(UNITS, abstractFood.getUnits());

        SQLiteDatabase dbs = getWritableDatabase();
        dbs.insert(ABSTRACT_TABLE, null, values);
        dbs.close();
    }

    public void addFoodInstanceToDatabase(Food food) {
        ContentValues values = new ContentValues(3);
        values.put(NAME, food.getAbstractFood().getName());
        values.put(DATE, food.getDate());
        values.put(QUANTITY, food.getQuantity());
        SQLiteDatabase dbs = getWritableDatabase();
        int ret = (int) dbs.insert(INVENTORY_TABLE, null, values);

        dbs.close();
    }

    public void addAllFoodInstancesToDatabase(List<Food> foods) {
        for (Food food : foods) {
            addFoodInstanceToDatabase(food);
        }
    }

    public List<AbstractFood> getAbstractFoodByCategory(String category) {
        List<Food> foodList = getAllFoodInstances();
        SQLiteDatabase dbs = getReadableDatabase();
        Cursor cursor = (Cursor) dbs.query(ABSTRACT_TABLE, new String[]{NAME, TYPE, CATEGORY, UNITS},
                null, new String[]{}, null, null, null);


        List<AbstractFood> abstractFoods = new ArrayList<>();
        while (cursor.moveToNext()) {
            String foodName = cursor.getString(cursor.getColumnIndex(NAME));
            FoodType foodType = FoodType.findFoodType(cursor.getString(cursor.getColumnIndex(TYPE)));
            FoodCategory foodCategory =
                    new FoodCategory(cursor.getString(cursor.getColumnIndex(CATEGORY)));
            String foodUnits = cursor.getString(cursor.getColumnIndex(UNITS));
            float totalQuantity = 0;
            for (Food food : foodList) {
                if (food.getAbstractFood().getName().equals(foodName)) {
                    totalQuantity += food.getQuantity();
                }
            }
            abstractFoods.add(new AbstractFood(foodName, foodCategory,
                    foodType, foodUnits, totalQuantity));
        }
        dbs.close();
        return abstractFoods;
    }

    private AbstractFood getAbstractFoodFromCursor(Cursor cursor) {

        String foodName = cursor.getString(cursor.getColumnIndex(NAME));
        FoodType foodType = FoodType.findFoodType(cursor.getString(cursor.getColumnIndex(TYPE)));
        FoodCategory foodCategory =
                new FoodCategory(cursor.getString(cursor.getColumnIndex(CATEGORY)));
        String foodUnits = cursor.getString(cursor.getColumnIndex(UNITS));

        return new AbstractFood(foodName, foodCategory, foodType, foodUnits);
    }

    public Cursor getJoinedTables() {
        String query = "SELECT * FROM " + ABSTRACT_TABLE +
                " NATURAL JOIN " + INVENTORY_TABLE;
        SQLiteDatabase dbs = getReadableDatabase();
        Cursor ret = dbs.rawQuery(query, new String[]{});
        dbs.close();
        return ret;
    }


    public void consumeFood(String name, Float quantity) {
        final List<Food> foodList = getFoodInstance(name);
        Collections.sort(foodList, new Comparator<Food>() {
            @Override
            public int compare(Food f1, Food f2) {
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("EEEE, yyyy-mm-dd");
                    Date parsedDate1 = formatter.parse(f1.getDate());
                    Date parsedDate2 = formatter.parse(f2.getDate());
                    return parsedDate1.compareTo(parsedDate2);

                } catch (ParseException e) {
                    return 0;
                }
            }
        });
        for (Food food : foodList) {
            float foodQuantity = food.getQuantity();
            if (foodQuantity <= quantity) {
                deleteRow(food);
                quantity =- foodQuantity;
            }
            else {
                updateRow(food, foodQuantity - quantity);
                return;
            }
        }
    }

    public void updateRow(Food food, float newQuantity) {
        ContentValues values = new ContentValues();
        String name = food.getAbstractFood().getName();
        values.put(NAME, name);
        values.put(DATE, food.getDate());
        values.put(QUANTITY, newQuantity);
        SQLiteDatabase dbs = getWritableDatabase();
        dbs.update(INVENTORY_TABLE, values, "name=?", new String[]{name});
        dbs.close();
    }


    private void deleteRow(Food food) {
        SQLiteDatabase dbs = getWritableDatabase();
        dbs.delete(INVENTORY_TABLE, "name=?", new String[]{food.getAbstractFood().getName()});
        dbs.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        throw new RuntimeException("onUpgrade method should not be called");
    }
}
