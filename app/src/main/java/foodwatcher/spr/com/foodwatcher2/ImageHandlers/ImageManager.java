package foodwatcher.spr.com.foodwatcher2.ImageHandlers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by mikefink on 2015-03-14.
 */
public class ImageManager {

    private static final String FOOD_FOLDER = "foodwatcher/food";
    private static final String RECIPE_FOLDER = "foodwatcher/recipes";

    private static FileOutputStream outputStream;

    public static void saveFoodImage(Bitmap bmp, String name) {
        saveImageToSD(bmp, name, FOOD_FOLDER);
    }

    public static void saveRecipeImage(Bitmap bmp, String name) {
        saveImageToSD(bmp, name, RECIPE_FOLDER);
    }

    private static void saveImageToSD(Bitmap bmp, String name, String folder) {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        File fileDir = new File(Environment.getExternalStorageDirectory()
                + File.separator + folder + File.separator);
        fileDir.mkdirs();
        File file = new File(fileDir, name + ".jpg");
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            try {
                outputStream = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return;
            }
            try {
                outputStream.write(bytes.toByteArray());
                outputStream.close();
                //Toast.makeText(this, "Image saved", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Bitmap retrieveImage(String type, String name) {
        // Type should be "food" or "recipes"
        String photoPath = Environment.getExternalStorageDirectory() +
                "/" + type + "/" + name + ".jpg";
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        final Bitmap b = BitmapFactory.decodeFile(photoPath, options);
        return b;
    }
}
