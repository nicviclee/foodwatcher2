package foodwatcher.spr.com.foodwatcher2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jwang on 15-03-14.
 */
public class Vegetables extends FoodType {
    private static Vegetables instance;

    public Vegetables() {
    }

    public static Vegetables getInstance() {
        if (instance != null)
            return instance;
        else {
            instance = new Vegetables();

            instance.setName("Vegetables");
            FoodCategory leafy = new FoodCategory("Leafy");
            FoodCategory mushrooms = new FoodCategory("Mushrooms");
            FoodCategory roots = new FoodCategory("Roots");
            FoodCategory bulb = new FoodCategory("Bulb");
            FoodCategory flower = new FoodCategory("Flower");
            FoodCategory seeded = new FoodCategory("Seeded");

            List<FoodCategory> items = new ArrayList<FoodCategory>();
            items.add(leafy);
            items.add(mushrooms);
            items.add(roots);
            items.add(bulb);
            items.add(flower);
            items.add(seeded);
            instance.setCategories(items);

            return instance;
        }
    }
}
