package foodwatcher.spr.com.foodwatcher2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class AddFoodItemFragment extends Fragment {
    private FoodType foodType;

    public AddFoodItemFragment() {
        // Required empty public constructor
    }

    public static Fragment createAddFoodItemFragment(FoodType type) {
        AddFoodItemFragment newFragment = new AddFoodItemFragment();
        newFragment.setFoodType(type);

        return newFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_food_item, container, false);

        FoodCategoryAdapter adapter = new FoodCategoryAdapter(getActivity(), foodType.getCategories());
        // attach to listview
        ListView listView = (ListView) rootView.findViewById(R.id.listview_add_food_item);
        listView.setAdapter(adapter);
        return rootView;
    }

    // getters and setters
    public FoodType getFoodType() {
        return this.foodType;
    }

    public void setFoodType(FoodType newType) {
        this.foodType = newType;
    }

}
